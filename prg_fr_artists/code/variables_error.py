age = 32 # An integer assignment
weight = 56.5 # A floating point
name = "Cathy" # A string
print(age)
print(weight)
print(name)

#Your assignment is to print these variables mentioned below.
declared_var = True                      # A boolean
some_list = [1,2,3,4,7]                  # List of only integers
another_list = [1.2, 4.5, 56, 7]         # A list of integers and floating point numbers
str_list = ["aviik", "daisy", "hancock"] # Alist of only Strings
somelist = [True, False, True, True]     # A list of booleans 
# This is a list of different data types
mixed_list = [True, "Mary", "Gurpreet", 32, 5.4]



#this is going to generate an error
print(undeclared_var)  # try to access an undefined variable
